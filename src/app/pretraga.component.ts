import { Component, OnInit } from '@angular/core';

import { ChuckService } from './chuck.service';

@Component({
  selector: 'chuck-search',
  templateUrl: `./pretraga.component.html`
})

export class PretragaComponent implements OnInit  {
	ChuckSearch = [];

	constructor(private chuckService: ChuckService) { }

	ngOnInit(): void {}

	search(query): void {
	  this.chuckService.getChuckSearch(query).then(ChuckSearch => this.ChuckSearch = ChuckSearch.slice(0, 10));
	}

	obris(i): void {
		this.ChuckSearch.splice(i, 1);
	}
}