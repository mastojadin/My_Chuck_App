"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var chuck_service_1 = require("./chuck.service");
var KategorijeComponent = (function () {
    function KategorijeComponent(chuckService) {
        this.chuckService = chuckService;
        this.ChuckCat = [];
    }
    KategorijeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.chuckService.getChuckAllCategory().then(function (AllCat) { return _this.AllCat = AllCat; });
    };
    KategorijeComponent.prototype.dajosh = function (category) {
        var _this = this;
        this.chuckService.getChuckCat(category).then(function (ChuckCat) { return _this.ChuckCat.unshift(ChuckCat); });
    };
    KategorijeComponent.prototype.obris = function (i) {
        this.ChuckCat.splice(i, 1);
    };
    return KategorijeComponent;
}());
KategorijeComponent = __decorate([
    core_1.Component({
        selector: 'chuck-category',
        templateUrl: "./kategorije.component.html"
    }),
    __metadata("design:paramtypes", [chuck_service_1.ChuckService])
], KategorijeComponent);
exports.KategorijeComponent = KategorijeComponent;
//# sourceMappingURL=kategorije.component.js.map