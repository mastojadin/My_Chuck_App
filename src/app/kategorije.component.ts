import { Component, OnInit } from '@angular/core';

import { ChuckService } from './chuck.service';

@Component({
  selector: 'chuck-category',
  templateUrl: `./kategorije.component.html`
})

export class KategorijeComponent implements OnInit  {
	AllCat;
	ChuckCat = [];

	constructor(private chuckService: ChuckService) { }

	ngOnInit(): void {
	  this.chuckService.getChuckAllCategory().then(AllCat => this.AllCat = AllCat);
	}

	dajosh(category): void {
	  this.chuckService.getChuckCat(category).then(ChuckCat => this.ChuckCat.unshift(ChuckCat))
	}

	obris(i): void {
		this.ChuckCat.splice(i, 1);
	}
}