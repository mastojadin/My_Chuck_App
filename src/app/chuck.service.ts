import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { ChuckRandom } from './vars';

@Injectable()
export class ChuckService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private chuckUrl = 'https://api.chucknorris.io/jokes';

  constructor(private http: Http) { }

  getChuckRandom(): Promise<ChuckRandom> {
    const url = `${this.chuckUrl}/random`;
    return this.http.get(url)
                    .toPromise()
                    .then(response => response.json().value as ChuckRandom)
                    .catch(this.handleError);
  }

  getChuckAllCategory(): Promise<AllCat[]> {
    const url = `${this.chuckUrl}/categories`;
    return this.http.get(url)
                    .toPromise()
                    .then(response => response.json() as AllCat[])
                    .catch(this.handleError);
  }

  getChuckCat(category): Promise<ChuckCat> {
    const url = `${this.chuckUrl}/random?category=${category}`;
    return this.http.get(url)
                    .toPromise()
                    .then(response => response.json().value as ChuckCat)
                    .catch(this.handleError);
  }

  getChuckSearch(query): Promise<ChuckSearch[]> {
    const url = `${this.chuckUrl}/search?query=${query}`;
    return this.http.get(url)
                    .toPromise()
                    .then(response => response.json().result as ChuckSearch[])
                    .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}