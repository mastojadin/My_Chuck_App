"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'My Chuck App';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        template: "\n  \t<div class=\"container text-center\">\n\t  \t<h1>{{title}}</h1>\n\t  \t\n\t  \t<a class=\"btn btn-info\" routerLink=\"/pochetna\" routerLinkActive=\"active\">Pochetna</a>\n\t  \t<a class=\"btn btn-info\" routerLink=\"/kategorije\" routerLinkActive=\"active\">Kategorije</a>\n\t  \t<a class=\"btn btn-info\" routerLink=\"/pretraga\" routerLinkActive=\"active\">Pretraga</a>\n    \n    \t<div class=\"h50\"></div>\n\n\t    <router-outlet></router-outlet>\n    </div>\n  "
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map