import { Component, OnInit } from '@angular/core';

import { ChuckService } from './chuck.service';

@Component({
  selector: 'chuck-home',
  templateUrl: `./pochetna.component.html`
})

export class PochetnaComponent implements OnInit  {
	ChuckRandom = [];

	constructor(private chuckService: ChuckService) { }

	ngOnInit(): void {
	  this.chuckService.getChuckRandom().then(ChuckRandom => this.ChuckRandom.push(ChuckRandom));
	}

	dajosh(): void {
	  this.chuckService.getChuckRandom().then(ChuckRandom => this.ChuckRandom.unshift(ChuckRandom));
	}

	obris(i): void {
		this.ChuckRandom.splice(i, 1);
	}
}