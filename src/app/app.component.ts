import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  	<div class="container text-center">
	  	<h1>{{title}}</h1>
	  	
	  	<a class="btn btn-info" routerLink="/pochetna" routerLinkActive="active">Pochetna</a>
	  	<a class="btn btn-info" routerLink="/kategorije" routerLinkActive="active">Kategorije</a>
	  	<a class="btn btn-info" routerLink="/pretraga" routerLinkActive="active">Pretraga</a>
    
    	<div class="h50"></div>

	    <router-outlet></router-outlet>
    </div>
  `
})

export class AppComponent  {
	title = 'My Chuck App';
}
