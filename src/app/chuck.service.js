"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var ChuckService = (function () {
    function ChuckService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.chuckUrl = 'https://api.chucknorris.io/jokes';
    }
    ChuckService.prototype.getChuckRandom = function () {
        var url = this.chuckUrl + "/random";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json().value; })
            .catch(this.handleError);
    };
    ChuckService.prototype.getChuckAllCategory = function () {
        var url = this.chuckUrl + "/categories";
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ChuckService.prototype.getChuckCat = function (category) {
        var url = this.chuckUrl + "/random?category=" + category;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json().value; })
            .catch(this.handleError);
    };
    ChuckService.prototype.getChuckSearch = function (query) {
        var url = this.chuckUrl + "/search?query=" + query;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json().result; })
            .catch(this.handleError);
    };
    ChuckService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return ChuckService;
}());
ChuckService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ChuckService);
exports.ChuckService = ChuckService;
//# sourceMappingURL=chuck.service.js.map