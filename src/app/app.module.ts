import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent }         from './app.component';
import { PochetnaComponent }   from './pochetna.component';
import { KategorijeComponent }   from './kategorije.component';
import { PretragaComponent }      from './pretraga.component';
import { ChuckService }          from './chuck.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    PochetnaComponent,
    KategorijeComponent,
    PretragaComponent
  ],
  providers: [ ChuckService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
