"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var chuck_service_1 = require("./chuck.service");
var PretragaComponent = (function () {
    function PretragaComponent(chuckService) {
        this.chuckService = chuckService;
        this.ChuckSearch = [];
    }
    PretragaComponent.prototype.ngOnInit = function () { };
    PretragaComponent.prototype.search = function (query) {
        var _this = this;
        this.chuckService.getChuckSearch(query).then(function (ChuckSearch) { return _this.ChuckSearch = ChuckSearch.slice(0, 10); });
    };
    PretragaComponent.prototype.obris = function (i) {
        this.ChuckSearch.splice(i, 1);
    };
    return PretragaComponent;
}());
PretragaComponent = __decorate([
    core_1.Component({
        selector: 'chuck-search',
        templateUrl: "./pretraga.component.html"
    }),
    __metadata("design:paramtypes", [chuck_service_1.ChuckService])
], PretragaComponent);
exports.PretragaComponent = PretragaComponent;
//# sourceMappingURL=pretraga.component.js.map