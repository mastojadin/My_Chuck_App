import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PochetnaComponent }   from './pochetna.component';
import { KategorijeComponent }   from './kategorije.component';
import { PretragaComponent }      from './pretraga.component';

const routes: Routes = [
  { path: '', redirectTo: '/pochetna', pathMatch: 'full' },
  { path: 'pochetna',  component: PochetnaComponent },
  { path: 'kategorije',  component: KategorijeComponent },
  { path: 'pretraga',     component: PretragaComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}